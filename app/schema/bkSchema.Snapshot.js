var mongoose = require('mongoose');

var snapshotSchema = mongoose.Schema({
    name : { type: String, required: true, trim: true }
});

module.exports = mongoose.model('Snapshot', snapshotSchema);
