module.exports = function (
    router,
// Schema
    Snapshot
) {
    'use strict';

    router.get('/', function (req, res) {
        res.json({ message: 'Please consult the docs to use this API!' });
    });

    // Sample Controller
    require('./controllers/bkController.API.Snapshot')(
        router,
        Snapshot
    );
};


