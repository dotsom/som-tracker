// This is how to do configurations in the future.  When using the require function,
// the return is always the module.export of the required module.

module.exports = {
    'url' : 'mongodb://127.0.0.1:27017/somTracker'
};
